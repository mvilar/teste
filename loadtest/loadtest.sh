#!/bin/bash

AB_LOG=/tmp/ab.log

# Verificar se o programa ab esta instalado
if ! ab -V > /dev/null 2>&1 ; then 
    echo "Este script requer o programa apache ab para funcionar. (Debian: apache2-utils)"
    exit 1
fi

loadtest() {
    url=$1
    increment=${2:-200}
    concurrency=${3:-1000}
    requests=${4:-50000}
    sec_limit=${5:-15}
    returnCode=0
    while [ $returnCode -eq 0 ]; do
        echo "Testando $requests requests com $concurrency conexoes simutaneas"
        ab -c $concurrency -n $requests -r -s $sec_limit $url > $AB_LOG 2>&1
        returnCode=$?
        if [ $returnCode -eq 0 ]; then
            lastConcurrency=$concurrency
            test && concurrency=$(($concurrency + $increment)) 
            sleep 0.2
        fi
    done
    echo "Foi realizado um total de $lastConcurrency simultaneas."
    #echo $returnCode
}

help() {
    echo -e "Voce deve informar o endereco para ser realizado o teste, conforme exemplo abaixo:"
    echo -e ""
    echo -e "$0 http://<IP|Hostname> [increment] [concurrency] [requests]"
    echo -e ""
    echo -e "Parametros Opcionais"
    echo -e "  increment   - Numero de conexoes que serao incrementadas em cada ciclo. (Padra: 200)"
    echo -e "  concurrency - Conexoes simultaneas no primeiro ciclo de testes. (Padrao: 1000)"
    echo -e "  requests    - Total de requestas a cada ciclo. (Padrao: 50000)"
    echo -e "  sec_limit   - Tempo limite em segundos para resposta do site. (Padrao: 15 segundos)"
}

case $1 in
    http://*)
        loadtest $@
    ;;
    *)
        help
    ;;
esac