#!/usr/bin/python
import smtplib
import yaml


def sendemail(from_addr, to_addr, subject, message, login, password, smtpserver):
    header = 'From: %s \n' % from_addr
    header += 'To: %s \n' % to_addr
    header += 'Content-Type: text/html; charset="utf-8"\n'
    header += 'Subject: %s \n\n' % subject
    message = header + message
    
    try:
        server = smtplib.SMTP(smtpserver)
        server.starttls()
        server.login(login,password)
        server.sendmail(from_addr, to_addr, message)
        server.quit()
    except Exception as error:
        print error


# Pegar configuracoes no formato yaml
configFile = "/opt/scripts/etc/email.conf"
with open(configFile, 'r') as configList:
    config = yaml.load(configList)


filter = config["log-search"]["filter"]
pathField = int(config["log-search"]["path-field"])
codeField = int(config["log-search"]["code-field"])
logPath = config["log-search"]["log-path"]
stats={}

with open(logPath, "r") as logFile:
    for line in logFile:
        if filter in line:
            splitLine = line.split()
            requests=splitLine[pathField]+" "+splitLine[codeField]
            if stats.get(requests):
                stats[requests]+=1
            else:
                stats[requests]=1


# Preparar mensagem para o email
msg = "Seguem as estatisticas de acesso do dia anterior:<br><br>"
msg += "<table><tr><td><b>Path</b></td><td><b>Return Code</b></td><td><b>Total Requests</b></td></tr>"
for key,value in stats.items():
    request=key.split()
    msg += ("<tr><td>%s</td><td>%s</td><td>%s</td></tr> \n" 
            % (request[0],request[1], value))
    print "%s \t %s " % (key, value)
msg += "</table>"    


# Validar se foram configurados os parametros de email e enviar
if config["email"]["sender"]:
    print "Enviando email..."
    sendemail(
            config["email"]["sender"],
            config["email"]["destination"],
            "Estatisticas Webserver Teste",
            msg,
            config["email"]["sender"],
            config["email"]["password"],
            config["email"]["smtp-server"]
        )

