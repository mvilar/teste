# Ansible para deploy de aplicação nodejs

Este diretorio contem roles para realizar o deploy de uma aplicação nodejs em
um servidor Ubuntu 16.04.


## Confgiuração

No diretório `inventories/_sample` está uma estrutura modelo para a execução do
deploy da aplicação de exemplo, altere o arquivo 
[inventories/_sample/hosts](inventories/_sample/hosts) com as configurações do
host onde será aplicado o deploy. 


## Preparação do servidor

Uma vez configurado o arquivo de hosts, execute a playbook `nodejs_server.yaml`
para instalação dos pacotes do nodejs, haproxy e outros, além da cópia dos
scripts:

```bash
ansible-playbook -i inventories/_sample/hosts nodejs_server.yaml
```


## Deploy

Realize o deploy da aplicação de teste executando a playbook
`nodejs_deploy.yaml` conforme o exemplo abaixo.

```bash
ansible-playbook -i inventories/_sample/hosts nodejs_deploy.yaml
```


## Rollback

Para realizar um rollback, execure a playbook `nodejs_rollback.yaml` conforme
abaixo:

```bash
ansible-playbook -i inventories/_sample/hosts nodejs_rollback.yaml
```