# 

## Receitas ansible para deploy do nodejs

As receitas encontram-se no diretorio `ansible`, mais detalhes sobre o 
funcionamento estão no arquivo [ansible/README.md](ansible/README.md).


## Teste de carga

No diretório `loadtest` encontra-se o script `loadtest.sh`, execute-o informando
o endereço do servidor web para realizar o teste de carga:

```bash
./loadtest.sh http://<IP>
```


## Estatisticas de log

O script de estatisticas é enviado para o servidor em uma das tasks do Ansible,
para utiliza-lo é necessario alterar o arquivo de configuração que está em
`/opt/scripts/etc/email.conf.sample`, removendo o sufixo .sample e alterando
as variaveis de configuracao do email, os parâmetros do filtro não precisam
ser alterados.

Uma vez configurado, execute o script pelo caminho:

```bash
/opt/scripts/parser-logs.py
```

*P.S.* O teste foi realizado com uma conta google, para isso requer liberação
de envio de emails por aplicativos menos seguros 
(https://myaccount.google.com/lesssecureapps).